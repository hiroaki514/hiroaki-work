<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('posts')->insert([['id' => '1','title' => 'タイトル１','body' => 'あああああああ',],['id' => '2','title' => 'タイトル2','body' => 'あああああああ',],['id' => '3','title' => 'タイトル3','body' => 'あああああああ',],['id' => '4','title' => 'タイトル4','body' => 'あああああああ',],['id' => '5','title' => 'タイトル5','body' => 'あああああああ',],]);
    }
}
