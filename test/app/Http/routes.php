<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
return view('welcome');
});
Route::get('/top', 'PostsController@index')->name('top');
Route::resource('posts', 'PostsController', ['only' => ['index','create', 'store', 'show', 'edit']]);
Route::post('/posts/delete/{id}', 'PostsController@destroy');
Route::post('/posts/edit/{id}', 'PostsController@update');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');
Route::resource('comments', 'CommentsController', ['only' => ['store']]);
