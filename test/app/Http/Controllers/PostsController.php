<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\Log;

class PostsController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->input('search');
        $posts = Post::orderBy('created_at', 'desc')->where('title','LIKE','%'.$keyword.'%')->orWhere('body','LIKE','%'.$keyword.'%')->paginate(10);
        return view('posts.index', ['posts' => $posts])->with(['keyword' => $keyword]);
    }
    
    public function show($post_id)
    {
        $post = Post::findOrFail($post_id);
    
        return view('posts.show', [
            'post' => $post,
        ]);
    }
    
    public function create()
    {
        return view('posts.create');
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
        ]);
        
        
        // $params = $request->validate([
            // 'title' => 'required|max:50',
            // 'body' => 'required|max:2000',
        // ]);
        
    
        // Post::create($params);
        Post::create([
            'title' => $request['title'],
            'body' => $request['body'],
        ]);
    
       return redirect()->route('posts.index');
    }
    
    public function edit($post_id)
{
    $post = Post::findOrFail($post_id);

    return view('posts.edit', [
        'post' => $post,
    ]);
}

public function update($post, Request $request)
{
    // $params = $request->validate([
    //     'title' => 'required|max:50',
    //     'body' => 'required|max:2000',
    // ]);
    $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

    $post = Post::findOrFail($post);

    // $post->fill($request)->save();
    $post->fill([
            'post_id' => $request['title'],
            'body' => $request['body'],
        ])->save();

    return redirect()->route('posts.show', ['post' => $post]);
}
public function destroy($post)
{
    $post = Post::findOrFail($post);

    \DB::transaction(function () use ($post) {
        $post->comments()->delete();
        $post->delete();
    });

    
    return redirect()->route('top');
}
}

