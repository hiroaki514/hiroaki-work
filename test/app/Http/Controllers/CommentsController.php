<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class CommentsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'required|exists:posts,id',
            'body' => 'required|max:2000',
        ]);

        $post = Post::findOrFail($request['post_id']);
        // $post->comments()->create($request);
        $post->comments()->create([
            'post_id' => $request['post_id'],
            'body' => $request['body'],
        ]);

        return redirect()->route('posts.show', ['post' => $post]);
    }
}